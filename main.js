
// var name = "rayan";
// console.log('name')
// name = "Forkan";
// console.log(name);


// function printName(){
//     var name = "Arif";
//     console.log(name);


// }
// printName();
// console.log(name);


//  var name = "forkan";
//  if(name == "forkan"){
//     var fullName = "forkan Ahmed"; 
// }
// console.log(fullName);

// let name = "Amir,Ameer";
// let name = "Ameer";
// console.log(name);
// console.log(name);

// let x = 1;

// if (x === 1) {
//   const x = 2;

//   console.log(x);

// }

// console.log(x);

// let person  = {
//     name: 'Nishu',
//     gender: 'female'

// }
// console.log(person.name);
// console.log(person.gender);

const integers = [1, 2, 3];

//output same
// const updateIntegers = integers.map(function(number){
//     return number += 2

//output same
//const updateIntegers = integers.map((number) => {
//return number += 1


//const updateIntegers = integers.map( number => number += 1);

const updateIntegers = integers.map(() => "Nishu");

console.log(updateIntegers);


//others

const ages = [42, 44, 7, 18];

//  const adults = ages.filter(function(age) {
//      return age > 20;

//  const adults = ages.filter((age) => {
//     return age > 20;

const adults = ages.filter((age) => age > 20);

console.log(adults);


//others

const name = "Nila";
const age = 35;

//const sentence = 'My name is ' + name + ', and ' + age +' years old';

//console.log(sentence);

const sentence = ' My name is name, and i am age years old';

console.log(sentence);



//others
// function Person(name, age) {
//     this.name = name;
//     this.age = age;
// };

// Person.prototype.speak = function () {
//     console.log(`My name is ${this.name}`);
// }

// const bill = new Person('Bill', 50)
// bill.speak();

///Class use of constructor

// class Person {

//     constructor(name, age, children) {
//         this.name = name;
//         this.age = age;
//         this.children = children;
//     }
//     speak() {
//         console.log(`My name is ${this.name}`);
//     }

//     birth(child) {
//         this.children.push(child);
//         return this.children;
//     }

// };

// const bill = new Person('Bill', 60, ['seen', 'steph']);
// bill.speak();
// bill.birth('jess');
// console.log(bill.children);

// Spread Operator
const names = ['Ami', 'tumi', 'she']
const moreNames = ['Amena','Fatema', 'Ahysha']

//console.log(...names);

//['ami','tmi','she','gio','Amena','Fatema', 'Ahysha']

// let allNames = []
// allNames = allNames.concat(names) //['Ami', 'tumi', 'she']
// allNames.push('Gio') //['ami','tmi','she','gio']
// allNames = allNames.concat(moreNames) ////['ami','tmi','she','gio','Amena','Fatema', 'Ahysha']
// console.log(allNames)

const allNames = [...names, 'Gio', ...moreNames]
console.log(allNames)